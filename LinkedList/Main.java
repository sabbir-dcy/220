public class Main {
  public static void main(String[] args) {
    LinkedList list = new LinkedList();
    // test linked list here
}

class LinkedList {
  Node head;
  Node tail;

  public void add(Object element) {
    Node n = new Node(element);

    if (head == null) {
      head = n;
      tail = n;
    }else {
      tail.next = n;
      tail = n;
    }
  }

  public void insert(Object element, int index) {
    Node newNode = new Node(element);
    if (index == 0) {
      newNode.next = head;
      head = newNode;
    }
    else {
      Node pred = nodeAt(index - 1);
      newNode.next = pred.next;
      pred.next = newNode;
    }
  }

  public Node nodeAt(int index) {
    if (index < 0 || index > Node.size) {
      throw new IndexOutOfBoundsException();
    }
    Node temp = head;
    for (int i = 0; i < index; i++, temp = temp.next);
    return temp;
  }

  public void rotateRight() {
    Node pred = null;
    Node temp = head;

    while (temp.next != null) {
      pred = temp;
      temp = temp.next;
    }

    temp.next = head;
    head = temp;
    pred.next = null;
  }

  public void printDuplicate() {
    boolean found = false;
    for (Node first = head; first != null; first = first.next) {
      for (Node second = first.next; second != null; second = second.next) {
        if (first.element == second.element) {
            System.out.println(first.element);
            found = true;
            break;
        }
      }
      if (found) {
        break;
      }
    }
  }

  public void print() {
    Node temp = head;
    for (; temp != null; temp = temp.next) {
      System.out.println(temp.element);
    }
  }

  public int size() {
    return Node.size;
  }
}

class Node {
  Object element;
  Node next;
  static int size;
  
  public Node(Object element) {
    this.element = element;
    size++;
  }
}